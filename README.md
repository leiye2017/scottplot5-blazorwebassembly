# ScottPlot5测试

## BlazorWebAssembly

### 步骤
1. 使用.Net8创建Web工程并选择混合渲染模式
2. Client工程引用如下三个包	
```xml
<PackageReference Include="ScottPlot.Blazor" Version="5.0.18" />
<PackageReference Include="SkiaSharp" Version="2.88.7" />
<PackageReference Include="SkiaSharp.Views.Blazor" Version="2.88.7" />
```
3. 写代码
4. 启动BlazorApp1工程

### 效果
![ScottPlot On Blazor WebAssembly](https://img-blog.csdnimg.cn/direct/269f67d7bb2f47608882df556b71eab0.gif#pic_center)

### 已知问题
1. 中文显示异常


## Avalonia ScottPlot
Avalonia是.Net跨平台的UI库,支持Windows,Linux,iOS,MacOS,Android;
ScottPlot为跨平台的UI控件库,支持Avalonia,Windows,Eto,WebAssembly等平台

### 步骤
1. 安装Avalonia For VS2022扩展
2. 新建名为AvaScottPlot的Avalonia Cross Platform项目
3. 将AvaScottPlot主工程及各个平台的启动工程改为.Net8
4. AvaScottPlot工程增加如下引用
```xml
     <PackageReference Include="ScottPlot.Avalonia" Version="5.0.20" />
```
5. 写代码
6. 启动AvaScottPlot.Desktop等工程调试
7. Linux端选择AvaScottPlot.Desktop发布为linux-x64选择单文件

### 效果
本人电脑配置i5-1035G1,浏览器:Chrome 120.0.6099.225,虚拟机:VMware Workstation 16.2.3 Player;曲线数量20条;
1. Avalonia On Windows  32FPS
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/a43d0ee978ba4d20aa3af5c032f3df81.gif#pic_center)

2. Avalonia On  WebAssembly FPS为1,最小化浏览器时不渲染
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/04329e646d744bc2a7157ef235dca414.gif#pic_center)

3.  Avalonia On Linux 在虚拟机中运行FPS为1
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/335ecfc962534ec0b83a7f1768c09f0a.gif#pic_center)

4.  Avalonia On Android
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/46da67ee3a504a0aa2a3269a627e8565.jpeg#pic_center)

### 已知问题
1. WebAssembly项目中文显示异常


## ScottPlot WindowsForm 

### 步骤
1. 创建.Net8 WindowsForm工程
2. 引用如下两个包
```xml
<PackageReference Include="ScottPlot.Avalonia" Version="5.0.20" />
<PackageReference Include="ScottPlot.WinForms" Version="5.0.20" />
```
3. 主界面增加FormsPlot控件设置Dock为Fill,并设置窗体最大化
4. 写代码
5. 启动WinForm工程

### 主要代码
```cs
        private void Form1_Load(object? sender, EventArgs e)
        {
            if (first)
            {
                StartPlot();
            }
            first = false;
        }
        private void StartPlot()
        {
            InitLines();
            if (timer == null)
            {
                timer = new(); // 每隔1秒触发一次
                timer.Interval = 1000;
                timer.Tick += TimerElapsed;
                timer.Start();
            }
        }

        private void InitLines()
        {
            if (plot == null) return;
            // PlotFont = Fonts.Detect("实时数据");
            plot.Plot.Axes.Title.Label.Text = "RealTime Data:实时数据";
            plot.Plot.Axes.Title.Label.FontName = PlotFont;

            plot.Plot.Clear();
            Lines = new LineModel[LineCount];
            DateTime start = DateTime.Now;
            for (int i = 0; i < LineCount; i++)
            {
                Lines[i] = new LineModel();
                Lines[i].Init(i + 1, plot.Plot, start);
            }
            plot.Plot.Benchmark.IsVisible = true;
        }

        public void UpdateData(DateTime ts, double[] data)
        {
            int Min = Math.Min(data.Length, Lines.Length);
            if (Min <= 0) return;

            for (int i = 0; i < Min; i++)
            {
                UpdateData(i, ts, data[i]);
            }
            Render();
        }

        private void UpdateData(int i, DateTime ts, double latestValue)
        {
            var tmp = Lines[i];
            tmp.UpdateData(ts, latestValue);
        }

        public void Render()
        {
            plot.Plot.Axes.AutoScale();
            plot.Refresh();
        }

        private void TimerElapsed(object? sender, EventArgs e)
        {
            //InvokeAsync(() =>
            double[] data = new double[LineCount];
            for (int i = 0; i < LineCount; i++)
            {
                data[i] = Generate.RandomData.RandomNumber(100);
            }
            UpdateData(DateTime.Now, data);
        }
```

### 效果
刚启动时FPS有80,稳定后约50FPS
![Windows Form 效果图](https://img-blog.csdnimg.cn/direct/a0dc65d00ff04168b00d6d2b82b80c64.gif#pic_center)

### 已知问题
1. Title不显示,可能设计就是这样的