﻿using ScottPlot;
using ScottPlot.Blazor;
using System.Text.RegularExpressions;

namespace BlazorApp1.Client.Pages;

public partial class ScottPlot5Test : IDisposable
{
    private System.Timers.Timer? timer;
    private ScottPlot.Blazor.BlazorPlot? plot;
    private int LineCount = 20;
    private static string PlotFont = "Noto Sans TC"; //Segoe UI; Noto Sans; SimSun;Noto Mono;
    private LineModel[]? Lines;

    class LineModel
    {
        private static Regex LabelReg = new Regex(@"(?<=:)\d+(\.\d+)?", RegexOptions.Compiled);
        public const int MaxPointCount = 100;
        public string? Label;
        public ScottPlot.Plottables.Scatter? sca; // SignalXY:7FPS;Scatter:16FPS
        public double[]? ys;
        public double[]? xs;
        public void Init(int i, Plot Plot, DateTime start)
        {
            Plot.Axes.Title.Label.FontName = PlotFont;
            xs = Enumerable.Repeat(start.ToOADate(), LineModel.MaxPointCount).ToArray();
            ys = new double[MaxPointCount];
            sca = Plot.Add.Scatter<double, double>(xs, ys);
            Label = sca.Label = $"Speed速度{i}:0 RPM";
            Plot.Legend.IsVisible = true;
            Plot.Legend.Font.Name = PlotFont;
            Plot.Axes.DateTimeTicks(Edge.Bottom);
        }

        public void UpdateData(DateTime ts, double latestValue)
        {
            if (xs == null || ys == null || sca == null || Label == null) return;

            Array.Copy(xs, 1, xs, 0, xs.Length - 1);
            xs[LineModel.MaxPointCount - 1] = ts.ToOADate();
            // 使用正则表达式替换冒号后的数字
            string result = LabelReg.Replace(Label, latestValue.ToString("F1"));
            sca.Label = result;
            Array.Copy(ys, 1, ys, 0, ys.Length - 1);
            ys[LineModel.MaxPointCount - 1] = latestValue;
        }
    }

    private void StartPlot()
    {
        InitLines();
        if (timer == null)
        {
            timer = new System.Timers.Timer(1000); // 每隔1秒触发一次
            timer.Elapsed += TimerElapsed;
            timer.AutoReset = true; // 设置为true，使定时器自动重复触发
            timer.Start();
        }
    }

    private void InitLines()
    {
        if (plot == null) return;
        // PlotFont = Fonts.Detect("实时数据");
        plot.Plot.Axes.Title.Label.Text = "RealTime Data:实时数据";
        plot.Plot.Axes.Title.Label.FontName = PlotFont;

        plot.Plot.Clear();
        Lines = new LineModel[LineCount];
        DateTime start = DateTime.Now;
        for (int i = 0; i < LineCount; i++)
        {
            Lines[i] = new LineModel();
            Lines[i].Init(i + 1, plot.Plot, start);
        }
        plot.Plot.Benchmark.IsVisible = true;
    }

    protected override void OnAfterRender(bool firstRender)
    {
        if (firstRender && plot != null)
        {
            StartPlot();
        }
    }

    public void UpdateData(DateTime ts, double[] data)
    {
        int Min = Math.Min(data.Length, Lines.Length);
        if (Min <= 0) return;

        for (int i = 0; i < Min; i++)
        {
            UpdateData(i, ts, data[i]);
        }
        Render();
    }

    private void UpdateData(int i, DateTime ts, double latestValue)
    {
        var tmp = Lines[i];
        tmp.UpdateData(ts, latestValue);
    }

    public void Render()
    {
        plot.Plot.Axes.AutoScale();
        plot.Refresh();
    }

    private void TimerElapsed(object? sender, System.Timers.ElapsedEventArgs e)
    {
        InvokeAsync(() =>
        {
            double[] data = new double[LineCount];
            for (int i = 0; i < LineCount; i++)
            {
                data[i] = Generate.RandomData.RandomNumber(100);
            }
            UpdateData(DateTime.Now, data);
        });
    }

    public void Dispose()
    {
        timer?.Stop();
        timer?.Dispose();
    }
}

